<?php
namespace Concrete\Package\HelloPoodle;

defined('C5_EXECUTE') or die('Access Denied');

use Concrete\Core\Package\Package;
use Concrete\Core\Block\BlockType\BlockType;

class Controller extends Package
{
    protected $pkgHandle = 'hello_poodle';
    protected $appVersionRequired = '5.7.5';
    protected $pkgVersion = '1.0';

    public function getPackageDescription()
    {
        return t('Say Hello to our Poodle');
    }

    public function getPackageName()
    {
        return t('Hello Poodle');
    }

    public function install()
    {
        $pkg = parent::install();
        BlockType::installBlockType('hello_poodle', $pkg);
    }
}
?>