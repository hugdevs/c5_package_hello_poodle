<?php     defined('C5_EXECUTE') or die("Access Denied.");
$al = Core::make('helper/concrete/asset_library');
print Core::make('helper/concrete/ui')->tabs(array(
    array('poodle-settings', t('Poodle Settings'), true),
    array('poodle-description', t('Poodle Package Description'))
));
?>

<div id="ccm-tab-content-poodle-settings" class="ccm-tab-content">
    <div class="ccm-google-map-block-container row">
        <div class="col-xs-12">
            <div class="form-group">
                <label class="control-label" for="title" name="title">Title
                    <i class="launch-tooltip fa fa-question-circle"
                       title="<?php echo t('If you would like to have a title then add it here.'); ?>"></i>
                </label>
                <?php echo $form->text('title', $mapObj->title); ?>
            </div>
        </div>
    </div><!-- / Row -->
    <div class="row">
        <img src="/packages/hello_poodle/blocks/hello_poodle/icon.png">
    </div>
</div>
<div id="ccm-tab-content-poodle-description" class="ccm-tab-content">
    <div class="ccm-google-map-block-container row">
        <div class="col-xs-12">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor
            invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo
            dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
            dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
            clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
        </div>
    </div><!-- / Row -->
</div> <!-- / poodle-settings tab -->
<script type="text/javascript">
    document.getElementById("title").focus();
</script>